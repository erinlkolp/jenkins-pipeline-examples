# jenkinsfile-examples

This is a collection of really small, really basic Jenkins Pipeline building blocks.

Each is a "full" Jenkins Pipeline script, encapsulated in a node block. My thought is that it'd be great for the beginner. :)

### Notes

As of 04/2017, these are tested working on a Cloudbees Jenkins Enterprise 1x installation.

### License and Authors

Author:: Erin L. Kolp (erinlkolpfoss@gmail.com)
